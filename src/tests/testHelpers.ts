import { getTemplate } from "../helpers.js"
import ProgressBar from "../progress_bar/ProgressBar"
import ProgressBarStore from "../progress_bar/Store"
import UpdateUI from "../UpdateUI"

export function createFormWithSections(numOfSections = 3, withProgressBar = false): HTMLFormElement {
  const form = document.createElement('form') as HTMLFormElement;

  for(let i = 0; i < numOfSections; i++) {
    const section = document.createElement('div') as HTMLDivElement;

    section.classList.add('sf__step-container');
    form.append(section);
  }

  
  if (withProgressBar) {
    createProgressBar(form);
  }
  
  document.body.append(form);
  
  return form;
}

export function appendInputsToSections(form: HTMLFormElement): HTMLFormElement {
  const stepContainers: Element[] = Array.from(form.getElementsByClassName('sf__step-container'))

  if (stepContainers.length != 3) {
    throw Error('Form must contain 3 step containers')
  }

  stepContainers.forEach((stepContainer, index) => {
    switch (index) {
      case 0:
        stepContainer.append(buildInputGroup('name', 'text', 'James Bond', true))
        break

      case 1:
        stepContainer.append(buildInputGroup('age', 'number', '35', true))
        break

      case 2:
        stepContainer.append(buildInputGroup('drink', 'text', 'Martini (shaken, not stirred)', true))
        break
    
      default:
        stepContainer.append()
        break
    }
    
    form.append(stepContainer)
  })

  return form
}

function buildInputGroup(id: string, type: string, value: string, withValues?: boolean) {
  // create an input
  const inputContainer = document.createElement('div') as HTMLDivElement

  const label = document.createElement('label') as HTMLLabelElement
  const labelText = id.charAt(0).toUpperCase() + id.slice(1);
  label.setAttribute('for', id)
  label.textContent = labelText

  const input = document.createElement('input') as HTMLInputElement
  input.setAttribute('type', type)
  input.id = id
  input.classList.add('sf__input')
  input.setAttribute('name', id)

  if (withValues) {
    input.value = value
  }

  inputContainer.append(label)
  inputContainer.append(input)

  return inputContainer
}

async function createProgressBar(form: HTMLFormElement): Promise<void> {
  const progressBar = document.createElement('div');
  progressBar.id = 'sf__progress-bar-container';
  progressBar.innerHTML = await getTemplate('progressBar.html');
  console.log('template: ',await getTemplate('progressBar.html'))

  const progressBarStore = ProgressBarStore.getInstance();
  progressBarStore.setProgressBar = new ProgressBar(progressBar);

  form.insertBefore(progressBar, form.querySelector('.sf__step-container'));

  UpdateUI.changeProgressBarLength();
}