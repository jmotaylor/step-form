import FormState from "./FormState.js";
import ProgressBarStore from "./progress_bar/Store.js";
export function changeProgressBarLength() {
    const formState = FormState.getInstance();
    const percentage = (formState.currentStep / formState.maxStep) * 100;
    const progressBarStore = ProgressBarStore.getInstance();
    progressBarStore.progressBar.innerBar.style.width = percentage + '%';
}
//# sourceMappingURL=uiFunctions.js.map