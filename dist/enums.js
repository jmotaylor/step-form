export var FormRegion;
(function (FormRegion) {
    FormRegion[FormRegion["Start"] = 0] = "Start";
    FormRegion[FormRegion["Middle"] = 1] = "Middle";
    FormRegion[FormRegion["End"] = 2] = "End";
})(FormRegion || (FormRegion = {}));
export var FormButton;
(function (FormButton) {
    FormButton[FormButton["Prev"] = 0] = "Prev";
    FormButton[FormButton["Next"] = 1] = "Next";
    FormButton[FormButton["Reset"] = 2] = "Reset";
    FormButton[FormButton["Submit"] = 3] = "Submit";
})(FormButton || (FormButton = {}));
export var InputType;
(function (InputType) {
    InputType[InputType["Input"] = 0] = "Input";
    InputType[InputType["Textarea"] = 1] = "Textarea";
})(InputType || (InputType = {}));
//# sourceMappingURL=enums.js.map