import { StepForm } from "../StepForm"
import { createFormWithSections } from "./testHelpers"
import ButtonStore from "../buttons/Store"
import FormState from "../FormState"
import { FormRegion } from "../enums"
import SectionStore from "../sections/Store"

test('Current step changes after next button is clicked', () => {
  FormState.deleteInstance();
  new StepForm(createFormWithSections());
  const formState = FormState.getInstance();
  
  expect(formState.currentStep).toBe(1);

  const buttonStore = ButtonStore.getInstance();
  buttonStore.nextBtn?.button.click();

  expect(formState.currentStep).toBe(2);
})

test('Current step changes after previous button is clicked', () => {
  FormState.deleteInstance()
  new StepForm(createFormWithSections())
  const formState = FormState.getInstance()

  expect(formState.currentStep).toBe(1)

  const buttonStore = ButtonStore.getInstance()
  buttonStore.nextBtn?.button.click()

  expect(formState.currentStep).toBe(2)

  buttonStore.prevBtn?.button.click()

  expect(formState.currentStep).toBe(1)
})

test('Step won\'t go beyond last page', () => {
  FormState.deleteInstance()
  new StepForm(createFormWithSections(4))
  const formState = FormState.getInstance()

  expect(formState.currentStep).toBe(1)

  const buttonStore = ButtonStore.getInstance()
  buttonStore.nextBtn?.button.click()
  buttonStore.nextBtn?.button.click()
  buttonStore.nextBtn?.button.click()

  expect(formState.currentStep).toBe(4)

  buttonStore.nextBtn?.button.click()

  expect(formState.currentStep).toBe(4)
})

test('Step won\'t go beyond last page even with confirmation section', () => {
  FormState.deleteInstance()
  ButtonStore.deleteInstance()

  new StepForm(createFormWithSections(3), {withConfirm: true})
  const formState = FormState.getInstance()

  expect(formState.currentStep).toBe(1)

  const buttonStore = ButtonStore.getInstance()
  buttonStore.nextBtn?.button.click()
  buttonStore.nextBtn?.button.click()
  buttonStore.nextBtn?.button.click()

  expect(formState.currentStep).toBe(4)

  buttonStore.nextBtn?.button.click()

  expect(formState.currentStep).toBe(4)
})

test('Step won\'t go before first page', () => {
  FormState.deleteInstance()
  new StepForm(createFormWithSections(3))
  const formState = FormState.getInstance()

  expect(formState.currentStep).toBe(1)

  const buttonStore = ButtonStore.getInstance()
  buttonStore.prevBtn?.button.click()

  expect(formState.currentStep).toBe(1)

  buttonStore.nextBtn?.button.click()

  expect(formState.currentStep).toBe(2)

  buttonStore.prevBtn?.button.click()
  buttonStore.prevBtn?.button.click()

  expect(formState.currentStep).toBe(1)
})

test('Step won\'t go before first page', () => {
  FormState.deleteInstance()
  ButtonStore.deleteInstance()

  new StepForm(createFormWithSections(2), {withConfirm: true})
  const formState = FormState.getInstance()

  expect(formState.currentStep).toBe(1)

  const buttonStore = ButtonStore.getInstance()
  buttonStore.prevBtn?.button.click()

  expect(formState.currentStep).toBe(1)

  buttonStore.nextBtn?.button.click()

  expect(formState.currentStep).toBe(2)

  buttonStore.prevBtn?.button.click()
  buttonStore.prevBtn?.button.click()

  expect(formState.currentStep).toBe(1)
})

test('maxStep is correct', () => {
  FormState.deleteInstance()
  new StepForm(createFormWithSections(4))
  const formState = FormState.getInstance()
  
  expect(formState.maxStep).toBe(4)
})

test('pages is correct', () => {
  FormState.deleteInstance()
  new StepForm(createFormWithSections(4))
  const sectionStore = SectionStore.getInstance()
  
  expect(sectionStore.sections?.length).toBe(4)
})

test('region changes correctly', () => {
  FormState.deleteInstance()
  new StepForm(createFormWithSections())
  const formState = FormState.getInstance()
  
  expect(formState.region).toBe(FormRegion.Start)

  const buttonStore = ButtonStore.getInstance()
  buttonStore.nextBtn?.button.click()

  expect(formState.region).toBe(FormRegion.Middle)

  buttonStore.nextBtn?.button.click()

  expect(formState.region).toBe(FormRegion.End)

  buttonStore.nextBtn?.button.click()

  expect(formState.region).toBe(FormRegion.End) // Clicking beyond last page

  buttonStore.prevBtn?.button.click()

  expect(formState.region).toBe(FormRegion.Middle)

  buttonStore.prevBtn?.button.click()

  expect(formState.region).toBe(FormRegion.Start)

  buttonStore.prevBtn?.button.click()

  expect(formState.region).toBe(FormRegion.Start) // Clicking before first page
})

test('region changes correctly even with confirmation section', () => {
  FormState.deleteInstance()
  ButtonStore.deleteInstance()

  new StepForm(createFormWithSections(2), {withConfirm: true})
  const formState = FormState.getInstance()
  
  expect(formState.region).toBe(FormRegion.Start)

  const buttonStore = ButtonStore.getInstance()
  buttonStore.nextBtn?.button.click()

  expect(formState.region).toBe(FormRegion.Middle)

  buttonStore.nextBtn?.button.click()

  expect(formState.region).toBe(FormRegion.End)

  buttonStore.nextBtn?.button.click()

  expect(formState.region).toBe(FormRegion.End) // Clicking beyond last page

  buttonStore.prevBtn?.button.click()

  expect(formState.region).toBe(FormRegion.Middle)

  buttonStore.prevBtn?.button.click()

  expect(formState.region).toBe(FormRegion.Start)

  buttonStore.prevBtn?.button.click()

  expect(formState.region).toBe(FormRegion.Start) // Clicking before first page
})

test('progress bar exists', () => {
  FormState.deleteInstance()
  ButtonStore.deleteInstance()

  new StepForm(createFormWithSections(2), {withConfirm: true, withProgress: true})
  // const formState = FormState.getInstance()

  expect(document.querySelector('#sf__progress-bar-container')).toBe(true)
});