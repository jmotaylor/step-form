export enum FormRegion {
  Start,
  Middle,
  End
}

export enum FormButton {
  Prev,
  Next,
  Reset,
  Submit
}

export enum InputType {
  Input,
  Textarea,
}