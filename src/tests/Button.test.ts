import { StepForm } from "../StepForm"
import { createFormWithSections, appendInputsToSections } from "./testHelpers"
import ButtonStore from "../buttons/Store"
import SectionStore from "../sections/Store"
import InputStore from "../inputs/Store"
import FormState from "../FormState"

test('Initial visibility of buttons are correct', () => {
  new StepForm(createFormWithSections())
  const buttonStore = ButtonStore.getInstance()

  expect(buttonStore.prevBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.prevBtn?.button.classList.contains('hide')).toBe(true)

  expect(buttonStore.nextBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.nextBtn?.button.classList.contains('hide')).toBe(false)

  expect(buttonStore.resetBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.resetBtn?.button.classList.contains('hide')).toBe(true)

  expect(buttonStore.submitBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.submitBtn?.button.classList.contains('hide')).toBe(true)
})

test('Visibility of buttons update correctly', () => {
  new StepForm(createFormWithSections())
  const buttonStore = ButtonStore.getInstance()

  buttonStore.nextBtn?.button.click()

  expect(buttonStore.prevBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.prevBtn?.button.classList.contains('hide')).toBe(false)

  expect(buttonStore.nextBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.nextBtn?.button.classList.contains('hide')).toBe(false)

  expect(buttonStore.resetBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.resetBtn?.button.classList.contains('hide')).toBe(true)  

  expect(buttonStore.submitBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.submitBtn?.button.classList.contains('hide')).toBe(true)  

  buttonStore.nextBtn?.button.click()

  expect(buttonStore.prevBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.prevBtn?.button.classList.contains('hide')).toBe(false)

  expect(buttonStore.nextBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.nextBtn?.button.classList.contains('hide')).toBe(true)

  expect(buttonStore.resetBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.resetBtn?.button.classList.contains('hide')).toBe(false)  

  expect(buttonStore.submitBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.submitBtn?.button.classList.contains('hide')).toBe(false)  
})

test('Visibility of buttons update correctly with confirmation section', () => {
  FormState.deleteInstance()

  new StepForm(createFormWithSections(2), {withConfirm: true})
  const buttonStore = ButtonStore.getInstance()

  buttonStore.nextBtn?.button.click()

  expect(buttonStore.prevBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.prevBtn?.button.classList.contains('hide')).toBe(false)

  expect(buttonStore.nextBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.nextBtn?.button.classList.contains('hide')).toBe(false)

  expect(buttonStore.resetBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.resetBtn?.button.classList.contains('hide')).toBe(true)    

  expect(buttonStore.submitBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.submitBtn?.button.classList.contains('hide')).toBe(true)    

  buttonStore.nextBtn?.button.click()

  expect(buttonStore.prevBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.prevBtn?.button.classList.contains('hide')).toBe(false)

  expect(buttonStore.nextBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.nextBtn?.button.classList.contains('hide')).toBe(true)

  expect(buttonStore.resetBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.resetBtn?.button.classList.contains('hide')).toBe(false)    

  expect(buttonStore.submitBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.submitBtn?.button.classList.contains('hide')).toBe(false)    
})

test('Attributes of buttons are correct', () => {
  new StepForm(createFormWithSections())
  const buttonStore = ButtonStore.getInstance()

  expect(buttonStore.prevBtn?.button.id).toBe('sf__prev-btn')
  expect(buttonStore.nextBtn?.button.id).toBe('sf__next-btn')
  expect(buttonStore.resetBtn?.button.id).toBe('sf__reset-btn')
  expect(buttonStore.submitBtn?.button.id).toBe('sf__submit-btn')

  expect(buttonStore.prevBtn?.button.type).toBe('button')
  expect(buttonStore.nextBtn?.button.type).toBe('button')
  expect(buttonStore.resetBtn?.button.type).toBe('button')
  expect(buttonStore.submitBtn?.button.type).toBe('submit')
})

test('Form resets when reset button is clicked', () => {
  SectionStore.deleteInstance()
  FormState.deleteInstance()
  ButtonStore.deleteInstance()
  
  let form = createFormWithSections()
  form = appendInputsToSections(form)
  new StepForm(form, {withConfirm: true})
  
  const formState = FormState.getInstance()
  const buttonStore = ButtonStore.getInstance()
  const numberOfInputSections = formState.maxStep - 1
  
  for (let i = 0; i < numberOfInputSections; i++) {
    buttonStore.nextBtn?.button.click()
  }

  buttonStore.resetBtn?.button.click()

  const inputStore = InputStore.getInstance()
  const inputs = inputStore.inputs

  // Inputs are empty
  inputs?.forEach((input) => {
    expect((input.input as HTMLInputElement).value).toBe("")
  })

  // Form step is back to 1 
  expect(formState.currentStep).toBe(1)

  // Section visibiltiy is correct
  const sectionStore = SectionStore.getInstance()
  sectionStore.sections?.forEach((section, index) => {
    index == 0
      ? expect(section.section.classList.contains('none')).toBe(false)
      : expect(section.section.classList.contains('none')).toBe(true)
  })

  // Button visibility is correct
  expect(buttonStore.prevBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.prevBtn?.button.classList.contains('hide')).toBe(true)
  
  expect(buttonStore.nextBtn?.button.classList.contains('show')).toBe(true)
  expect(buttonStore.nextBtn?.button.classList.contains('hide')).toBe(false)

  expect(buttonStore.resetBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.resetBtn?.button.classList.contains('hide')).toBe(true)

  expect(buttonStore.submitBtn?.button.classList.contains('show')).toBe(false)
  expect(buttonStore.submitBtn?.button.classList.contains('hide')).toBe(true)
})