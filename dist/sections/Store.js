export default class SectionStore {
    constructor() { }
    set setSections(sections) {
        this.sections = sections;
    }
    get getLastSection() {
        if (this.sections) {
            return this.sections[this.sections.length - 1];
        }
        else {
            return null;
        }
    }
    static getInstance() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new SectionStore();
        return this.instance;
    }
    static deleteInstance() {
        this.instance = null;
    }
}
//# sourceMappingURL=Store.js.map